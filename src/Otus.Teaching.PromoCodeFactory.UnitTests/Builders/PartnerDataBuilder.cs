﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public static class PartnerDataBuilder
    {
        public static Partner CreatePartner()
        {
            return new Partner()
            {
                Id = Guid.Parse("c7a732d7-e939-4be9-b4aa-0d65e9d5917b"),
                IsActive = true,
                Name = "Test partner",
                NumberIssuedPromoCodes = 0,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("59ef13e8-1801-458c-b2cc-b05aaf7852b7"),
                        Limit = 10,
                        CreateDate = new DateTime(2021,1,1),
                        EndDate = new DateTime(2022,1,1)
                    }
                }
            };
        }
        
        public static Partner SetInactive(this Partner partner)
        {
            partner.IsActive = false;
            return partner;
        }

        public static Partner SetLimit(this Partner partner, int count = 10, bool limitIsActive = true)
        {
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>()
            {
                new PartnerPromoCodeLimit()
                {
                    Id = Guid.Parse("59ef13e8-1801-458c-b2cc-b05aaf7852b7"),
                    Limit = count,
                    CreateDate = new DateTime(2021,1,1),
                    EndDate = new DateTime(2022,1,1),
                    CancelDate = GetCancelDate(limitIsActive)
                }
            };
            return partner;
        }
        
        private static DateTime? GetCancelDate(bool limitIsActive)
        {
            if (limitIsActive)
                return null;
            else
                return new DateTime(2021, 1, 1);
        }

        public static Partner SetIssuedPromoCodes(this Partner partner, int issuedPromoCodes)
        {
            partner.NumberIssuedPromoCodes = issuedPromoCodes;
            return partner;
        }
    }
}