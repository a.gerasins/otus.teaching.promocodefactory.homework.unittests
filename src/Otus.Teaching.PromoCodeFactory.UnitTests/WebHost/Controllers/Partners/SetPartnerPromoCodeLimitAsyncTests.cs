﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnerRepositoryMock;
        private readonly PartnersController _partnerController;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnerRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnerController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_WrongPartner_NotFound()
        {
            var partner = PartnerDataBuilder.CreatePartner();
            var request = LimitRequestBuilder.CreateLimit();

            _partnerRepositoryMock.Setup(r => r.GetByIdAsync(partner.Id))
                .ReturnsAsync(() => null );

            var result = await _partnerController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            result.Should()
                .BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerNotActive_BadRequest()
        {
            var partner = PartnerDataBuilder.CreatePartner()
                .SetInactive();
            var request = LimitRequestBuilder.CreateLimit();

            _partnerRepositoryMock.Setup(r => r.GetByIdAsync(partner.Id))
                .ReturnsAsync(() => partner);

            var result = await _partnerController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            result.Should()
                .BeAssignableTo<BadRequestObjectResult>();
        }

        [Theory]
        [InlineData(true, 100, 0)]
        [InlineData(false, 100, 100)]
        public async Task SetPartnerPromoCodeLimitAsync_SetLimitToPartner_ClearIssuedPromoCodes(
            bool activeLimit, 
            int partnerPromoCodesCountAtStart,
            int partnerPromoCodesCountExprected)
        {
            var partner = PartnerDataBuilder.CreatePartner()
                .SetIssuedPromoCodes(partnerPromoCodesCountAtStart)
                .SetLimit(limitIsActive: activeLimit);
            var request = LimitRequestBuilder.CreateLimit();

            _partnerRepositoryMock.Setup(r => r.GetByIdAsync(partner.Id)).ReturnsAsync(() => partner);

            var result = await _partnerController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            partner.NumberIssuedPromoCodes.Should()
                .Be(partnerPromoCodesCountExprected);
        }
        
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_SetLimitToPartner_DisableLastLimit()
        {
            var partner = PartnerDataBuilder.CreatePartner()
                .SetIssuedPromoCodes(100)
                .SetLimit(limitIsActive: true);
            var limit = partner.PartnerLimits.First();
            var request = LimitRequestBuilder.CreateLimit();

            _partnerRepositoryMock.Setup(r => r.GetByIdAsync(partner.Id))
                .ReturnsAsync(() => partner);

            var result = await _partnerController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            limit.CancelDate.HasValue.Should()
                .Be(true);
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_SetWrongLimit_BadRequest()
        {
            var partner = PartnerDataBuilder.CreatePartner();
            var request = LimitRequestBuilder.CreateLimit()
                .SetWrongLimit();

            _partnerRepositoryMock.Setup(r => r.GetByIdAsync(partner.Id))
                .ReturnsAsync(() => partner);

            var result = await _partnerController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            result.Should()
                .BeAssignableTo<BadRequestObjectResult>();
        }

        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_NewLimit_SavedToDb()
        {
            var partner = PartnerDataBuilder.CreatePartner();
            var request = LimitRequestBuilder.CreateLimit();

            _partnerRepositoryMock.Setup(r => r.GetByIdAsync(partner.Id))
                .ReturnsAsync(() => partner);

            var result = await _partnerController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            _partnerRepositoryMock.Verify(r => r.UpdateAsync(partner), Times.Once);
        }
    }
}